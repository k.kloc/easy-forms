'use strict';

const path = require('path');
const fs = require('fs');
const google = require('@googleapis/forms');
const {authenticate} = require('@google-cloud/local-auth');
const {OAuth2Client} = require('google-auth-library');
const formID = '1wXZu195AVHmfiN3LrJJrW2i5DbpamoxlVizFvHwWl-U';

let clientAuthPath = path.join(__dirname, 'auth_credentials.json');
let clientKeysPath = path.join(__dirname, 'client_keys.json')
let responsesDataPath = path.join(__dirname, '../responses_data.json');
let clientAuthData = null;

async function loadCachedData() {
    try {
        let credentials = JSON.parse(fs.readFileSync(clientAuthPath, 'utf8'));
        let keys = JSON.parse(fs.readFileSync(clientKeysPath, 'utf8'))["installed"];
        clientAuthData = new OAuth2Client(
            keys["client_id"],
            keys["client_secret"],
            keys["redirect_uris"][0]
        );
        clientAuthData.setCredentials(credentials);
    } catch (err) {
        clientAuthData = null;
    }
}

async function authenticateClient() {
    clientAuthData = await authenticate({
        keyfilePath: clientKeysPath,
        scopes: [
            'https://www.googleapis.com/auth/forms',
            'https://www.googleapis.com/auth/drive',
            'https://www.googleapis.com/auth/forms.body.readonly'
        ],
        access_type: 'offline',
    });

    fs.writeFileSync(clientAuthPath, JSON.stringify(clientAuthData.credentials));
}

async function getAnswers() {
    await loadCachedData()
    if (clientAuthData == null) {
        await authenticateClient();
    }

    const forms = google.forms({
        version: 'v1',
        auth: clientAuthData,
    });
    const metadata = await forms.forms.get({formId: formID});

    const items = metadata.data.items;
    const itemMap = new Map();
    items.forEach(item => {
        const {title, questionItem} = item;
        itemMap.set(title, questionItem.question.questionId);
    });

    const answerAPI = await forms.forms.responses.list({formId: formID});
    const responsesAPI = answerAPI.data.responses;

    let responses = [];
    responsesAPI.forEach(response => {
        const responseMap = new Map();
        const answers = response.answers;

        for (const [k, v] of itemMap.entries()) {
            if (answers[v]['textAnswers'] == null) {
                continue;
            }
            responseMap.set(k, answers[v]['textAnswers']['answers'][0]['value']);
        }

        const responseJson = Object.fromEntries(responseMap);
        responses.push(responseJson);
    });

    fs.writeFileSync(responsesDataPath, JSON.stringify(Array.from(responses)));

    return responses;
}

if (module === require.main) {
    getAnswers().catch(console.error);
}
module.exports = getAnswers;
