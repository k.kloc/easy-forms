<?php
session_start();
// Check if the user is authenticated
    if (!isset($_SESSION["authenticated"]) || $_SESSION["authenticated"] !== true) {
        // User is not authenticated, redirect to the login page
        header("Location: /admin");
        exit();
}
;

require('refresh_data.php');

?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dane z formularza</title>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="forms-panel/form-analyze-script.js"></script>
    <link rel="stylesheet" href="forms-panel/form-analyze-styles.css">
</head>

<body>

    <br></br>

    <!-- Menu for sorting -->
    <div class="sort container">
        <label for="sort-key">Posortuj po: </label>
        <select id="sort-key" onchange="renderChartTable()">
            <!-- Sort options will be here -->
        </select>
    </div>

    <br></br>

    <div id="forms-container">
        <!-- Forms will be dynamically inserted here -->
    </div>

    <div id="chart-container" style="width: 60%; margin: auto;">
        <canvas id="bar-chart">
            <!-- Bar graph will be dynamically inserted here  -->
        </canvas>
    </div>

    <table id="data-table" border="1">
        <!-- Table will be dynamically inserted here -->
    </table>

</body>
</html>