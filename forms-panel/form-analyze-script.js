
// Render options for the sorting select menu.
function renderSortOptions(jsonData) {
    var keys = Object.keys(jsonData[0]);
    var select = document.getElementById('sort-key');
    keys.forEach(function(key) {
        var option = document.createElement('option');
        option.value = key;
        option.textContent = key.charAt(0).toUpperCase() + key.slice(1);
        select.appendChild(option);
    });
}

function renderTable(jsonData) {
    var sortKey = document.getElementById('sort-key').value;

    // Create a map to group data by the selected key.
    var groupedData = {};
    jsonData.forEach(function(item) {
        var groupKey = item[sortKey];
        if (!groupedData[groupKey]) {
            groupedData[groupKey] = [];
        }
        groupedData[groupKey].push(item);
    });

    // Check if 'email' is one of the table headers.
    // We only want to provide "Send email" option if emails where provided in
    // the form.
    var hasEmail = Object.keys(jsonData[0]).includes('email');

    // Create the table rows based on sorted and grouped data, adding "Send email" button if email column exists.
    var rowsHtml = '';
    // Create forms for sending email if email column exists.
    var formsHtml = '';
  
    Object.keys(groupedData).sort().forEach(function(group) {
        var groupName = group.replace(/\s/g, '-');
        
        // Add a header and "Send email" button for the group
        rowsHtml += '<tr class="group-header"><th colspan="100%">'
                    + '<div class="group-header-content">'
                    + '<span class="group-name">' + group + '</span>'
                    + (hasEmail ? ' <button onclick="sendEmailsForGroup(\'' + groupName + '\')">Wyślij maile</button>' : '')
                    + '</div></th></tr>';

        // Add rows for the group.
        rowsHtml += groupedData[group].map(function(item) {
            return '<tr>' + Object.keys(item).filter(function(key) {
                return key !== sortKey; // Exclude the sortKey in the data rows.
            }).map(function(key) {
                return '<td>' + item[key] + '</td>';
            }).join('') + '</tr>';
        }).join('');

        // If email column exists, prepare the email forms.
        // Each form should have a unique form id and input id.
        if (hasEmail) {
            var emailList = groupedData[group].map(item => item.email).join(',');
            formsHtml += '<form id="form-' + groupName + '" method="post" action="/forms-panel/send-email-to-list.php" style="display:none;">'
                        + '<input type="hidden" id="input-' + groupName + '" name="send_email_to_list" value="' + emailList + '">'
                        + '</form>';
        }
    });

    // Render the table and forms.
    document.getElementById('data-table').innerHTML = rowsHtml;
    document.getElementById('forms-container').innerHTML = formsHtml;
}

function sendEmailsForGroup(group) {
    var formId = 'form-' + group;
    document.getElementById(formId).submit();
}

let barChart = null;

function renderChart(data, labels) {
    // Destroy bar chart if it already exists.
    if (barChart) {
        barChart.destroy();
    }

    const ctx = document.getElementById('bar-chart').getContext('2d');

    // Create a new bar chart.
    barChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: labels,
            datasets: [{
                label: 'Liczba',
                data: data,
                backgroundColor: 'rgba(255, 99, 132, 0.2)',
                borderColor: 'rgba(255, 99, 132, 1)',
                borderWidth: 1
            }]
        },
        options: {
            indexAxis: 'x',
            scales: {
                y: {
                    beginAtZero: true,
                    // Ensure that only integers are displayed as ticks.
                    ticks: {
                        stepSize: 1,
                        callback: function(value) {
                            if (value % 1 === 0) {
                                return value;
                            }
                        }
                    }
                }
            }
        }
    });
}

function createChartData(jsonData) {
    var sortKey = document.getElementById('sort-key').value;

    const groupedData = jsonData.reduce(function(acc, currentValue) {
        const groupKey = currentValue[sortKey];
        if (!acc[groupKey]) {
            acc[groupKey] = 0;
        }
        acc[groupKey]++;
        return acc;
    }, {});

    renderChart(Object.values(groupedData), Object.keys(groupedData));
}

// Render graph and table when sorting option changed.
function renderChartTable() {
    fetch('/forms-panel/responses_data.json')
    .then(response => response.json())
    .then(data => {
        createChartData(data);
        renderTable(data);
    })
}

// Initial setup - render sort options, graph and table.
function init() {
    fetch('/forms-panel/responses_data.json')
    .then(response => response.json())
    .then(data => {
        renderSortOptions(data);
        createChartData(data);
        renderTable(data);
    })
}

window.onload = init;
