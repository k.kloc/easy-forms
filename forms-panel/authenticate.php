<?php
$validUsername = "user"; // Replace with your actual username
$validPassword = "password"; // Replace with your actual password

session_start();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $username = $_POST["uname"];
    $password = $_POST["psw"];

    if ($username == $validUsername && $password == $validPassword) {
        // Authentication successful, redirect to the secure panel
        $_SESSION["authenticated"] = true;
        header("Location: /admin-panel");
        exit();
    } else {
        // Authentication failed, redirect back to the login page with an error message
        header("Location: login.html?error=1");
        exit(); 
        // todo log error
    }
}
else {
    header("Location: /admin");
    exit();
}
?>